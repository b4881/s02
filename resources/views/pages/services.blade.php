@extends('layouts/app_old')

@section('content')
   <h1>{{ $title }}</h1>
   <p>this website is the services section</p>
   @if(count($services) > 0)
       @foreach($services as $service)
           <li> {{ $service }}</li>
       @endforeach
   @else
       <p>Nothing to Display</p>
   @endif
@endsection

