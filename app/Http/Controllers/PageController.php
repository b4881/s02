<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function hello()
    {
        /*return view('hello')->with('name', 'Homer Simpson');*/
        $info = array(
            'front_end' => 'Zuitt programs',
            'topics' => ['html', 'css', 'js', 'vue']
        );

        return view('hello')->with($info);

    }
    public function index()
    {
        $title = 'Welcome to Laravel';
        return view('/pages/index')->with('title', $title);

    }
    public function about()
    {
        $title = 'About page';
        return view('/pages/about')->with('title', $title);

    }
    public function services()
    {
        $data = array(
            'title' => 'Services page',
            'services' => ['Web Design', 'Development', 'SEO']
        );
        return view('/pages/services')->with($data);;

    }
}
