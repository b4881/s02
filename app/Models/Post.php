<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //This means that a post has a relationship with a user and it belongs to a user. We could also say that a single post belongs to a user.
    public function posts(){

        return $this -> belongsTo('App\Models\User');

    }
}
